package com.logachev.customviewexample1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    HorizontalLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (HorizontalLayout) findViewById(R.id.container);
        /**
         * Добавляем в контейнер еще одну вью программно.
         */
        Button button = new Button(this);
        button.setText("Button");
        button.setLayoutParams(container.generateLayoutParams(2));
        container.addView(button);
    }
}
